import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

void Initialing() async {
  await Firebase.initializeApp();
}

void stateFirebase(context){
  FirebaseAuth.instance
      .authStateChanges()
      .listen((User user) {
    if(user == null){
      print('User is currently singe out');
    }else{
      Navigator.pushNamed(context, 'NewsList');
    }
  });
}

 register({@required email, @required password}) async {
  try {
    UserCredential userCredential = await FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: email,
        password: password,
    );
  } on FirebaseAuthException catch (e) {
    if (e.code == 'weak-password') {
      return Future.error('La contraseña es muy debil');
    } else if (e.code == 'email-already-in-use') {
      return Future.error('El correo ya esta en uso');
    }
  } catch (e) {
    print(e);
  }
}

 singIn({@required email, @required password}) async {
  var passwordWrong = '1';
  var userNotFound = '2';
  try {
    UserCredential userCredential = await FirebaseAuth.instance.signInWithEmailAndPassword(
        email: email,
        password: password
    );
  } on FirebaseAuthException catch (e) {
    if (e.code == 'user-not-found') {
      return Future.error('user-not-found');
    } else if (e.code == 'wrong-password') {
      return Future.error('wrong-password');
    }
  }
}

 singOut() async {
  await FirebaseAuth.instance.signOut();
}

