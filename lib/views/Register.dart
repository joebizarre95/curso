
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter_tutorial_project/Utils/FirebaseController.dart' as  firebaseController;


class RegisterView extends StatefulWidget {
  @override
  _RegisterViewState createState() => _RegisterViewState();
}

class _RegisterViewState extends State<RegisterView> {
  @override
  Widget build(BuildContext context) {
    var _Heigth = MediaQuery.of(context).size.height;
    var _Width = MediaQuery.of(context).size.width;
    bool LogButton = false;
    TextEditingController _textEditingControllerEmail = new TextEditingController();
    TextEditingController _textEditingControllerPass = new TextEditingController();


    return Scaffold(
        body: Container(
            child: Stack(
              children: [
                Column(
                  children: [
                    Container(
                      padding: EdgeInsets.only(
                          top: 40,
                          left: 25,
                          right: 25
                      ),
                      height: _Heigth,
                      width: _Width,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                              "Registrar nuevo usuario",
                              style: GoogleFonts.signika(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 20.0,
                              )
                          ),
                          Image.asset('assets/imgs/BitLogo.png',
                              width: 80 ,
                              height: 40,
                              fit:BoxFit.contain),
                          Padding(
                            padding: EdgeInsets.all(10.0),
                          ),
                          Align(
                            alignment: Alignment.center,
                            child: TextField(
                              controller:_textEditingControllerEmail,
                              cursorColor: Colors.pink,
                              decoration: InputDecoration(
                                prefixIcon: Icon(
                                    Icons.person,
                                    color: Colors.pinkAccent
                                ),
                                hintText: 'Username',
                                border: OutlineInputBorder(
                                    borderRadius:  BorderRadius.circular(30)
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(30),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: _Heigth * 0.02,
                          ),
                          Align(
                            alignment: Alignment.center,
                            child: TextField(
                              controller: _textEditingControllerPass,
                              obscureText: true,
                              cursorColor: Colors.pink,
                              decoration: InputDecoration(
                                prefixIcon: Icon(
                                    Icons.lock,
                                    color: Colors.pinkAccent
                                ),
                                hintText: 'Password',
                                border: OutlineInputBorder(
                                    borderRadius:  BorderRadius.circular(30)
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(30),
                                ),
                              ),
                            ),
                          ),
                          Padding(padding: EdgeInsets.only(top:30,)),
                          Align(
                            alignment: Alignment.center,
                            child: ButtonTheme(
                                minWidth: _Width,
                                height: 50,
                                child: RaisedButton(
                                  onPressed: () {
                                    setState(() {
                                      if(_textEditingControllerEmail.text.isEmpty || _textEditingControllerPass.text.isEmpty) {
                                        AlertDialog alert = AlertDialog(
                                          content: Text('porfavor llene todos los parametros'),
                                        );
                                        showDialog(
                                          context: context,
                                          builder: (BuildContext context) {
                                            return alert;
                                          },
                                        );
                                      }else {
                                        bool emailValid = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(_textEditingControllerEmail.text);
                                        if(emailValid) {
                                          var controller = firebaseController.singIn(email: _textEditingControllerEmail.text,
                                              password: _textEditingControllerPass.text);
                                          firebaseController.stateFirebase(context);
                                        }else{
                                          AlertDialog alert = AlertDialog(
                                            content: Text('porfavor inserte un correo valido Ej: correo@correo.com'),
                                          );
                                          showDialog(
                                            context: context,
                                            builder: (BuildContext context) {
                                              return alert;
                                            },
                                          );
                                        }
                                      }
                                    });
                                  },
                                  child: Text(
                                    'REGISTRAR',
                                    style: TextStyle(fontSize: 18),
                                    textAlign: TextAlign.center,
                                  ),
                                  color: Colors.white,
                                  shape: RoundedRectangleBorder(
                                      side: BorderSide(color: Colors.black12),
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(20,)
                                      )
                                  ),
                                ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top:20)
                          ),
                          Align(
                            alignment: Alignment.center,
                            child: ButtonTheme(
                            minWidth: _Width,
                            height: 45,
                            buttonColor: Colors.white,
                            child: RaisedButton(
                              shape: RoundedRectangleBorder(
                                  side: BorderSide(color: Colors.black12),
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(20,)
                                  )
                              ),
                               child: Text('VOLVER',
                                 style: TextStyle(fontSize: 18),
                                 textAlign: TextAlign.center,),
                               onPressed: () {
                                 Navigator.pop(context);
                               },
                             )
                            )
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            )
        )
    );
  }
}

