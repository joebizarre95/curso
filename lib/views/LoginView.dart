
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter_tutorial_project/Utils/FirebaseController.dart' as  firebaseController;


class LoginView extends StatefulWidget {
    @override
    _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  @override
  Widget build(BuildContext context) {
    var _Heigth = MediaQuery.of(context).size.height;
    var _Width = MediaQuery.of(context).size.width;
    bool LogButton = false;
    TextEditingController _textEditingControllerEmail = new TextEditingController();
    TextEditingController _textEditingControllerPass = new TextEditingController();


    return Scaffold(
      body: Container(
        child: Stack(
          children: [
            Container(
              width: _Width,
              height: _Heigth,
              decoration: BoxDecoration(
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: AssetImage('assets/imgs/backbit.png'),
                )
              ),
            ),
            Container(
              width:  _Width,
              height: _Heigth,
              color: Colors.white.withAlpha(1000)
            ),
            Column(
              children: [
                Container(
                  padding: EdgeInsets.only(
                    top: 40,
                    left: 25,
                    right: 25
                  ),
                  height: _Heigth * 0.65,
                  width: _Width,
                  decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black,
                        blurRadius: 15
                      ),
                    ],
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(130),
                      bottomRight: Radius.circular(130),
                    )
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Welcome to",
                        style: GoogleFonts.signika(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 20.0,
                        )
                      ),
                      Image.asset('assets/imgs/BitLogo.png',
                                  width: 80 ,
                                  height: 40,
                          fit:BoxFit.contain),
                      Text(
                        'Please login to continue',
                        style:GoogleFonts.signika(
                          fontSize: 16,
                          color: Colors.black,
                          fontWeight: FontWeight.bold
                        )
                      ),
                      Padding(
                        padding: EdgeInsets.all(10.0),
                      ),
                      Align(
                        alignment: Alignment.center,
                        child: TextField(
                          controller:_textEditingControllerEmail,
                          cursorColor: Colors.pink,
                          decoration: InputDecoration(
                            prefixIcon: Icon(
                              Icons.person,
                              color: Colors.pinkAccent
                            ),
                            hintText: 'Username',
                            border: OutlineInputBorder(
                              borderRadius:  BorderRadius.circular(30)
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: _Heigth * 0.02,
                      ),
                      Align(
                        alignment: Alignment.center,
                        child: TextField(
                          controller: _textEditingControllerPass,
                          obscureText: true,
                          cursorColor: Colors.pink,
                          decoration: InputDecoration(
                            prefixIcon: Icon(
                                Icons.lock,
                                color: Colors.pinkAccent
                            ),
                            hintText: 'Password',
                            border: OutlineInputBorder(
                                borderRadius:  BorderRadius.circular(30)
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30),
                            ),
                          ),
                        ),
                      ),
                      Align(
                        alignment: Alignment.centerRight,
                        child: TextButton(
                          onPressed: () {
                             AlertDialog alert = AlertDialog(
                              title: Text('Ingrese su correo para recuperar su contraseña'),
                              content: TextField(
                                onChanged: (value) {},
                                decoration: InputDecoration(hintText: "correo"),
                              ),
                              actions: [
                                FlatButton(
                                  color: Colors.white,
                                  textColor: Colors.black,
                                  child: Text('CANCELAR'),
                                  onPressed: () {
                                    setState(() {
                                      Navigator.pop(context);
                                    });
                                  },
                                ),
                                FlatButton(
                                  color: Colors.pinkAccent,
                                  textColor: Colors.white,
                                  child: Text('ACEPTAR'),
                                  onPressed: () {
                                    setState(() {
                                      Navigator.pop(context);
                                    });
                                  },
                                )
                              ],
                            );
                             showDialog(
                               context: context,
                               builder: (BuildContext context) {
                                 return alert;
                               },
                             );
                          },
                          child: Text(
                            'Forgot password?',
                            style: TextStyle(
                              color: Colors.pink,
                            ),
                          )
                        )
                      ),
                      Align(
                        alignment: Alignment.center,
                        child: ButtonTheme(
                          minWidth: _Width,
                          height: 60,
                          child: RaisedButton(
                            onPressed: () {
                              setState(() {
                                if(_textEditingControllerEmail.text.isEmpty || _textEditingControllerPass.text.isEmpty) {
                                  AlertDialog alert = AlertDialog(
                                    content: Text('porfavor llene todos los parametros'),
                                  );
                                  showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return alert;
                                    },
                                  );
                                }else {
                                  bool emailValid = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(_textEditingControllerEmail.text);
                                  if(emailValid) {
                                    var controller = firebaseController.singIn(email: _textEditingControllerEmail.text,
                                        password: _textEditingControllerPass.text);
                                    firebaseController.stateFirebase(context);
                                  }else{
                                    AlertDialog alert = AlertDialog(
                                      content: Text('porfavor inserte un correo valido Ej: correo@correo.com'),
                                    );
                                    showDialog(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return alert;
                                      },
                                    );
                                  }

                                }
                              });
                            },
                            child: Text(
                              'LOGIN',
                              style: TextStyle(fontSize: 20),
                              textAlign: TextAlign.center,
                            ),
                            color: Colors.white,
                            shape: RoundedRectangleBorder(
                                side: BorderSide(color: Colors.black12),
                                borderRadius: BorderRadius.all(
                                    Radius.circular(20,)
                                )
                            ),
                          )
                        ),
                      )
                    ],
                  ),
                ),
                Expanded( flex: 500, child: SizedBox()),
                Text('OR', style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 30,
                  color: Colors.black54,
                )),
                Expanded(flex: 1000, child:SizedBox()),
                RaisedButton(onPressed: (){
                  Navigator.pushNamed(context, 'Register');
                },
                      child:Text('SINGUP'),
                      color: Colors.white,
                      shape: RoundedRectangleBorder(
                          side: BorderSide(color: Colors.black12),
                          borderRadius: BorderRadius.all(
                              Radius.circular(20,)
                          ))),
                Expanded(flex: 1000, child:SizedBox()),
                Text('Elaborated by Santiago Sanchez :) @')
              ],
            ),
          ],
        )
      )
    );
  }
}

