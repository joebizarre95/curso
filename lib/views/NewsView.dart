import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_tutorial_project/presenter/build_view.dart';
import 'package:flutter_tutorial_project/Utils/FirebaseController.dart' as  firebaseController;

class NewsView extends StatefulWidget {
  @override
  _NewsView createState() => _NewsView();
}

class _NewsView extends State<NewsView> {
  int Newsint = 1;
  String viewName = 'NewsList';
  static const drawerOptions = <Map<String, dynamic>>[
    {
      'value': 1,
      'text':
      'All articles about Tesla from the last month, sorted by recent first'
    },
    {'value': 2, 'text': 'Top business headlines in the US right now'},
    {
      'value': 3,
      'text':
      'All articles mentioning Apple from yesterday, sorted by popular publishers first'
    },
    {'value': 4, 'text': 'Top headlines from TechCrunch right now'},
    {
      'value': 5,
      'text':
      'All articles published by the Wall Street Journal in the last 6 months, sorted by recent first'
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.pinkAccent),
        title: Text(
          'BIT News',
            style: TextStyle(color: Colors.pinkAccent),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 10),
            child: Icon(Icons.favorite),
          )
        ],
      ),

      drawer: Drawer(
        child: Column(
          children: [
            DrawerHeader(child: CircleAvatar(
              radius: 70,
              backgroundColor: Colors.white,
              child: Image(
                image: AssetImage('assets/imgs/BitLogo.png'),
              )
            )),
            ...drawerOptions
                .map((option) =>
            ListTile(
              title: Text(option['text']),
                      onTap: () {
                      setState(() {
                        Newsint = option['value'];
                        Navigator.pop(context);
                      });
                  }
            )).toList(),
            ListTile(
                title: Text('Sing out'),
                onTap: () {
                  firebaseController.singOut();
                  Navigator.of(context).pushNamedAndRemoveUntil('login', (route) => false);
            })
          ]
        )
      ),
      body: Container(
        padding: EdgeInsets.only(right: 10, left: 10),
        child: BuildView(viewName: viewName, intNews: Newsint)
      ),
    );
  }
}